/**
 * @fileoverview This file is generated by the Angular template compiler.
 * Do not edit.
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride}
 */
 /* tslint:disable */


import * as import0 from '@angular/core';
import * as import1 from '@angular/common';
import * as import2 from '../../../../../../node_modules/ng-jhipster/src/language/jhi-translate.directive.ngfactory';
import * as import3 from 'ng-jhipster/src/language/jhi-translate.directive';
import * as import4 from '@angular/forms';
import * as import5 from 'ng-jhipster/src/pipe/order-by.pipe';
import * as import6 from 'ng-jhipster/src/pipe/pure-filter.pipe';
import * as import7 from '../../../../../../../../src/main/webapp/app/admin/configuration/configuration.component';
import * as import8 from '../../../../../../../../src/main/webapp/app/admin/configuration/configuration.service';
const styles_JhiConfigurationComponent:any[] = ([] as any[]);
export const RenderType_JhiConfigurationComponent:import0.RendererType2 = import0.ɵcrt({
  encapsulation: 2,
  styles: styles_JhiConfigurationComponent,
  data: {}
}
);
function View_JhiConfigurationComponent_3(l:any):import0.ɵViewDefinition {
  return import0.ɵvid(0,[
      (l()(),import0.ɵeld(0,(null as any),(null as any),11,'div',[[
        'class',
        'row'
      ]
    ],(null as any),(null as any),(null as any),(null as any),(null as any))),
    (l()(),import0.ɵted((null as any),['\n                    '])),
      (l()(),import0.ɵeld(0,(null as any),(null as any),1,'div',[[
        'class',
        'col-md-4'
      ]
    ],(null as any),(null as any),(null as any),(null as any),(null as any))),
    (l()(),import0.ɵted((null as any),[
      '',
      ''
    ]
    )),
    (l()(),import0.ɵted((null as any),['\n                    '])),
      (l()(),import0.ɵeld(0,(null as any),(null as any),5,'div',[[
        'class',
        'col-md-8'
      ]
    ],(null as any),(null as any),(null as any),(null as any),(null as any))),
    (l()(),import0.ɵted((null as any),['\n                        '])),
      (l()(),import0.ɵeld(0,(null as any),(null as any),2,'span',[[
        'class',
        'float-right badge badge-default break'
      ]
    ],(null as any),(null as any),(null as any),(null as any),(null as any))),
    (l()(),import0.ɵted((null as any),[
      '',
      ''
    ]
    )),
    import0.ɵpid(0,import1.JsonPipe,([] as any[])),
    (l()(),import0.ɵted((null as any),['\n                    '])),
    (l()(),import0.ɵted((null as any),['\n                ']))
  ]
  ,(null as any),(ck,v) => {
    const currVal_0:any = v.context.$implicit;
    ck(v,3,0,currVal_0);
    const currVal_1:any = import0.ɵunv(v,8,0,import0.ɵnov(v,9).transform((<any>v.parent).context.$implicit.properties[v.context.$implicit]));
    ck(v,8,0,currVal_1);
  });
}
function View_JhiConfigurationComponent_2(l:any):import0.ɵViewDefinition {
  return import0.ɵvid(0,[
    (l()(),import0.ɵeld(0,(null as any),(null as any),11,'tr',([] as any[]),(null as any),(null as any),(null as any),(null as any),(null as any))),
    (l()(),import0.ɵted((null as any),['\n            '])),
    (l()(),import0.ɵeld(0,(null as any),(null as any),2,'td',([] as any[]),(null as any),(null as any),(null as any),(null as any),(null as any))),
    (l()(),import0.ɵeld(0,(null as any),(null as any),1,'span',([] as any[]),(null as any),(null as any),(null as any),(null as any),(null as any))),
    (l()(),import0.ɵted((null as any),[
      '',
      ''
    ]
    )),
    (l()(),import0.ɵted((null as any),['\n            '])),
    (l()(),import0.ɵeld(0,(null as any),(null as any),4,'td',([] as any[]),(null as any),(null as any),(null as any),(null as any),(null as any))),
    (l()(),import0.ɵted((null as any),['\n                '])),
    (l()(),import0.ɵand(16777216,(null as any),(null as any),1,(null as any),View_JhiConfigurationComponent_3)),
    import0.ɵdid(802816,(null as any),0,import1.NgForOf,[
      import0.ViewContainerRef,
      import0.TemplateRef,
      import0.IterableDiffers
    ]
      ,{ngForOf: [
        0,
        'ngForOf'
      ]
    },(null as any)),
    (l()(),import0.ɵted((null as any),['\n            '])),
    (l()(),import0.ɵted((null as any),['\n        ']))
  ]
  ,(ck,v) => {
    var co:any = v.component;
    const currVal_1:any = co.keys(v.context.$implicit.properties);
    ck(v,9,0,currVal_1);
  },(ck,v) => {
    const currVal_0:any = v.context.$implicit.prefix;
    ck(v,4,0,currVal_0);
  });
}
function View_JhiConfigurationComponent_5(l:any):import0.ɵViewDefinition {
  return import0.ɵvid(0,[
    (l()(),import0.ɵeld(0,(null as any),(null as any),10,'tr',([] as any[]),(null as any),(null as any),(null as any),(null as any),(null as any))),
    (l()(),import0.ɵted((null as any),['\n                '])),
      (l()(),import0.ɵeld(0,(null as any),(null as any),1,'td',[[
        'class',
        'break'
      ]
    ],(null as any),(null as any),(null as any),(null as any),(null as any))),
    (l()(),import0.ɵted((null as any),[
      '',
      ''
    ]
    )),
    (l()(),import0.ɵted((null as any),['\n                '])),
      (l()(),import0.ɵeld(0,(null as any),(null as any),4,'td',[[
        'class',
        'break'
      ]
    ],(null as any),(null as any),(null as any),(null as any),(null as any))),
    (l()(),import0.ɵted((null as any),['\n                    '])),
      (l()(),import0.ɵeld(0,(null as any),(null as any),1,'span',[[
        'class',
        'float-right badge badge-default break'
      ]
    ],(null as any),(null as any),(null as any),(null as any),(null as any))),
    (l()(),import0.ɵted((null as any),[
      '',
      ''
    ]
    )),
    (l()(),import0.ɵted((null as any),['\n                '])),
    (l()(),import0.ɵted((null as any),['\n            ']))
  ]
  ,(null as any),(ck,v) => {
    const currVal_0:any = v.context.$implicit.key;
    ck(v,3,0,currVal_0);
    const currVal_1:any = v.context.$implicit.val;
    ck(v,8,0,currVal_1);
  });
}
function View_JhiConfigurationComponent_4(l:any):import0.ɵViewDefinition {
  return import0.ɵvid(0,[
    (l()(),import0.ɵeld(0,(null as any),(null as any),26,'div',([] as any[]),(null as any),(null as any),(null as any),(null as any),(null as any))),
    (l()(),import0.ɵted((null as any),['\n        '])),
    (l()(),import0.ɵeld(0,(null as any),(null as any),2,'label',([] as any[]),(null as any),(null as any),(null as any),(null as any),(null as any))),
    (l()(),import0.ɵeld(0,(null as any),(null as any),1,'span',([] as any[]),(null as any),(null as any),(null as any),(null as any),(null as any))),
    (l()(),import0.ɵted((null as any),[
      '',
      ''
    ]
    )),
    (l()(),import0.ɵted((null as any),['\n        '])),
      (l()(),import0.ɵeld(0,(null as any),(null as any),19,'table',[[
        'class',
        'table table-sm table-striped table-bordered table-responsive d-table'
      ]
    ],(null as any),(null as any),(null as any),(null as any),(null as any))),
    (l()(),import0.ɵted((null as any),['\n            '])),
    (l()(),import0.ɵeld(0,(null as any),(null as any),10,'thead',([] as any[]),(null as any),(null as any),(null as any),(null as any),(null as any))),
    (l()(),import0.ɵted((null as any),['\n            '])),
    (l()(),import0.ɵeld(0,(null as any),(null as any),7,'tr',([] as any[]),(null as any),(null as any),(null as any),(null as any),(null as any))),
    (l()(),import0.ɵted((null as any),['\n                '])),
      (l()(),import0.ɵeld(0,(null as any),(null as any),1,'th',[[
        'class',
        'w-40'
      ]
    ],(null as any),(null as any),(null as any),(null as any),(null as any))),
    (l()(),import0.ɵted((null as any),['Property'])),
    (l()(),import0.ɵted((null as any),['\n                '])),
      (l()(),import0.ɵeld(0,(null as any),(null as any),1,'th',[[
        'class',
        'w-60'
      ]
    ],(null as any),(null as any),(null as any),(null as any),(null as any))),
    (l()(),import0.ɵted((null as any),['Value'])),
    (l()(),import0.ɵted((null as any),['\n            '])),
    (l()(),import0.ɵted((null as any),['\n            '])),
    (l()(),import0.ɵted((null as any),['\n            '])),
    (l()(),import0.ɵeld(0,(null as any),(null as any),4,'tbody',([] as any[]),(null as any),(null as any),(null as any),(null as any),(null as any))),
    (l()(),import0.ɵted((null as any),['\n            '])),
    (l()(),import0.ɵand(16777216,(null as any),(null as any),1,(null as any),View_JhiConfigurationComponent_5)),
    import0.ɵdid(802816,(null as any),0,import1.NgForOf,[
      import0.ViewContainerRef,
      import0.TemplateRef,
      import0.IterableDiffers
    ]
      ,{ngForOf: [
        0,
        'ngForOf'
      ]
    },(null as any)),
    (l()(),import0.ɵted((null as any),['\n            '])),
    (l()(),import0.ɵted((null as any),['\n        '])),
    (l()(),import0.ɵted((null as any),['\n    ']))
  ]
  ,(ck,v) => {
    var co:any = v.component;
    const currVal_1:any = co.allConfiguration[v.context.$implicit];
    ck(v,23,0,currVal_1);
  },(ck,v) => {
    const currVal_0:any = v.context.$implicit;
    ck(v,4,0,currVal_0);
  });
}
function View_JhiConfigurationComponent_1(l:any):import0.ɵViewDefinition {
  return import0.ɵvid(0,[
    (l()(),import0.ɵeld(0,(null as any),(null as any),49,'div',([] as any[]),(null as any),(null as any),(null as any),(null as any),(null as any))),
    (l()(),import0.ɵted((null as any),['\n    '])),
      (l()(),import0.ɵeld(0,(null as any),(null as any),2,'h2',[[
        'jhiTranslate',
        'configuration.title'
      ]
    ],(null as any),(null as any),(null as any),import2.View_JhiTranslateComponent_0,import2.RenderType_JhiTranslateComponent)),
      import0.ɵdid(49152,(null as any),0,import3.JhiTranslateComponent,([] as any[]),{jhiTranslate: [
        0,
        'jhiTranslate'
      ]
    },(null as any)),
    (l()(),import0.ɵted((null as any),['Configuration'])),
    (l()(),import0.ɵted((null as any),['\n\n    '])),
      (l()(),import0.ɵeld(0,(null as any),(null as any),2,'span',[[
        'jhiTranslate',
        'configuration.filter'
      ]
    ],(null as any),(null as any),(null as any),import2.View_JhiTranslateComponent_0,import2.RenderType_JhiTranslateComponent)),
      import0.ɵdid(49152,(null as any),0,import3.JhiTranslateComponent,([] as any[]),{jhiTranslate: [
        0,
        'jhiTranslate'
      ]
    },(null as any)),
    (l()(),import0.ɵted((null as any),['Filter (by prefix)'])),
    (l()(),import0.ɵted((null as any),[' '])),
    (l()(),import0.ɵeld(0,(null as any),(null as any),5,'input',[
      [
        'class',
        'form-control'
      ]
      ,
      [
        'type',
        'text'
      ]

    ]
    ,[
      [
        2,
        'ng-untouched',
        (null as any)
      ]
      ,
      [
        2,
        'ng-touched',
        (null as any)
      ]
      ,
      [
        2,
        'ng-pristine',
        (null as any)
      ]
      ,
      [
        2,
        'ng-dirty',
        (null as any)
      ]
      ,
      [
        2,
        'ng-valid',
        (null as any)
      ]
      ,
      [
        2,
        'ng-invalid',
        (null as any)
      ]
      ,
      [
        2,
        'ng-pending',
        (null as any)
      ]

    ]
    ,[
      [
        (null as any),
        'ngModelChange'
      ]
      ,
      [
        (null as any),
        'input'
      ]
      ,
      [
        (null as any),
        'blur'
      ]
      ,
      [
        (null as any),
        'compositionstart'
      ]
      ,
      [
        (null as any),
        'compositionend'
      ]

    ]
    ,(v,en,$event) => {
      var ad:boolean = true;
      var co:any = v.component;
      if (('input' === en)) {
        const pd_0:any = ((<any>import0.ɵnov(v,11)._handleInput($event.target.value)) !== false);
        ad = (pd_0 && ad);
      }
      if (('blur' === en)) {
        const pd_1:any = ((<any>import0.ɵnov(v,11).onTouched()) !== false);
        ad = (pd_1 && ad);
      }
      if (('compositionstart' === en)) {
        const pd_2:any = ((<any>import0.ɵnov(v,11)._compositionStart()) !== false);
        ad = (pd_2 && ad);
      }
      if (('compositionend' === en)) {
        const pd_3:any = ((<any>import0.ɵnov(v,11)._compositionEnd($event.target.value)) !== false);
        ad = (pd_3 && ad);
      }
      if (('ngModelChange' === en)) {
        const pd_4:any = ((<any>(co.filter = $event)) !== false);
        ad = (pd_4 && ad);
      }
      return ad;
    },(null as any),(null as any))),
    import0.ɵdid(16384,(null as any),0,import4.DefaultValueAccessor,[
      import0.Renderer,
      import0.ElementRef,
      [
        2,
        import4.COMPOSITION_BUFFER_MODE
      ]

    ]
    ,(null as any),(null as any)),
    import0.ɵprd(1024,(null as any),import4.NG_VALUE_ACCESSOR,(p0_0:any) => {
      return [p0_0];
    },[import4.DefaultValueAccessor]),
    import0.ɵdid(671744,(null as any),0,import4.NgModel,[
      [
        8,
        (null as any)
      ]
      ,
      [
        8,
        (null as any)
      ]
      ,
      [
        8,
        (null as any)
      ]
      ,
      [
        2,
        import4.NG_VALUE_ACCESSOR
      ]

    ]
      ,{model: [
        0,
        'model'
      ]
    },{update: 'ngModelChange'}),
    import0.ɵprd(2048,(null as any),import4.NgControl,(null as any),[import4.NgModel]),
    import0.ɵdid(16384,(null as any),0,import4.NgControlStatus,[import4.NgControl],(null as any),(null as any)),
    (l()(),import0.ɵted((null as any),['\n    '])),
    (l()(),import0.ɵeld(0,(null as any),(null as any),1,'label',([] as any[]),(null as any),(null as any),(null as any),(null as any),(null as any))),
    (l()(),import0.ɵted((null as any),['Spring configuration'])),
    (l()(),import0.ɵted((null as any),['\n    '])),
      (l()(),import0.ɵeld(0,(null as any),(null as any),25,'table',[[
        'class',
        'table table-striped table-bordered table-responsive d-table'
      ]
    ],(null as any),(null as any),(null as any),(null as any),(null as any))),
    (l()(),import0.ɵted((null as any),['\n        '])),
    (l()(),import0.ɵeld(0,(null as any),(null as any),14,'thead',([] as any[]),(null as any),(null as any),(null as any),(null as any),(null as any))),
    (l()(),import0.ɵted((null as any),['\n        '])),
    (l()(),import0.ɵeld(0,(null as any),(null as any),11,'tr',([] as any[]),(null as any),(null as any),(null as any),(null as any),(null as any))),
    (l()(),import0.ɵted((null as any),['\n            '])),
      (l()(),import0.ɵeld(0,(null as any),(null as any),3,'th',[[
        'class',
        'w-40'
      ]
      ],(null as any),[[
        (null as any),
        'click'
      ]
    ],(v,en,$event) => {
      var ad:boolean = true;
      var co:any = v.component;
      if (('click' === en)) {
        co.orderProp = 'prefix';
        const pd_0:any = ((<any>(co.reverse = !co.reverse)) !== false);
        ad = (pd_0 && ad);
      }
      return ad;
    },(null as any),(null as any))),
      (l()(),import0.ɵeld(0,(null as any),(null as any),2,'span',[[
        'jhiTranslate',
        'configuration.table.prefix'
      ]
    ],(null as any),(null as any),(null as any),import2.View_JhiTranslateComponent_0,import2.RenderType_JhiTranslateComponent)),
      import0.ɵdid(49152,(null as any),0,import3.JhiTranslateComponent,([] as any[]),{jhiTranslate: [
        0,
        'jhiTranslate'
      ]
    },(null as any)),
    (l()(),import0.ɵted((null as any),['Prefix'])),
    (l()(),import0.ɵted((null as any),['\n            '])),
      (l()(),import0.ɵeld(0,(null as any),(null as any),3,'th',[[
        'class',
        'w-60'
      ]
      ],(null as any),[[
        (null as any),
        'click'
      ]
    ],(v,en,$event) => {
      var ad:boolean = true;
      var co:any = v.component;
      if (('click' === en)) {
        co.orderProp = 'properties';
        const pd_0:any = ((<any>(co.reverse = !co.reverse)) !== false);
        ad = (pd_0 && ad);
      }
      return ad;
    },(null as any),(null as any))),
      (l()(),import0.ɵeld(0,(null as any),(null as any),2,'span',[[
        'jhiTranslate',
        'configuration.table.properties'
      ]
    ],(null as any),(null as any),(null as any),import2.View_JhiTranslateComponent_0,import2.RenderType_JhiTranslateComponent)),
      import0.ɵdid(49152,(null as any),0,import3.JhiTranslateComponent,([] as any[]),{jhiTranslate: [
        0,
        'jhiTranslate'
      ]
    },(null as any)),
    (l()(),import0.ɵted((null as any),['Properties'])),
    (l()(),import0.ɵted((null as any),['\n        '])),
    (l()(),import0.ɵted((null as any),['\n        '])),
    (l()(),import0.ɵted((null as any),['\n        '])),
    (l()(),import0.ɵeld(0,(null as any),(null as any),6,'tbody',([] as any[]),(null as any),(null as any),(null as any),(null as any),(null as any))),
    (l()(),import0.ɵted((null as any),['\n        '])),
    (l()(),import0.ɵand(16777216,(null as any),(null as any),3,(null as any),View_JhiConfigurationComponent_2)),
    import0.ɵdid(802816,(null as any),0,import1.NgForOf,[
      import0.ViewContainerRef,
      import0.TemplateRef,
      import0.IterableDiffers
    ]
      ,{ngForOf: [
        0,
        'ngForOf'
      ]
    },(null as any)),
    import0.ɵppd(3),
    import0.ɵppd(3),
    (l()(),import0.ɵted((null as any),['\n        '])),
    (l()(),import0.ɵted((null as any),['\n    '])),
    (l()(),import0.ɵted((null as any),['\n    '])),
    (l()(),import0.ɵand(16777216,(null as any),(null as any),1,(null as any),View_JhiConfigurationComponent_4)),
    import0.ɵdid(802816,(null as any),0,import1.NgForOf,[
      import0.ViewContainerRef,
      import0.TemplateRef,
      import0.IterableDiffers
    ]
      ,{ngForOf: [
        0,
        'ngForOf'
      ]
    },(null as any)),
    (l()(),import0.ɵted((null as any),['\n']))
  ]
  ,(ck,v) => {
    var co:any = v.component;
    const currVal_0:any = 'configuration.title';
    ck(v,3,0,currVal_0);
    const currVal_1:any = 'configuration.filter';
    ck(v,7,0,currVal_1);
    const currVal_9:any = co.filter;
    ck(v,13,0,currVal_9);
    const currVal_10:any = 'configuration.table.prefix';
    ck(v,28,0,currVal_10);
    const currVal_11:any = 'configuration.table.properties';
    ck(v,33,0,currVal_11);
    const currVal_12:any = import0.ɵunv(v,41,0,ck(v,43,0,import0.ɵnov((<any>v.parent),0),import0.ɵunv(v,41,0,ck(v,42,0,import0.ɵnov((<any>v.parent),1),co.configuration,co.filter,'prefix')),co.orderProp,co.reverse));
    ck(v,41,0,currVal_12);
    const currVal_13:any = co.keys(co.allConfiguration);
    ck(v,48,0,currVal_13);
  },(ck,v) => {
    const currVal_2:any = import0.ɵnov(v,15).ngClassUntouched;
    const currVal_3:any = import0.ɵnov(v,15).ngClassTouched;
    const currVal_4:any = import0.ɵnov(v,15).ngClassPristine;
    const currVal_5:any = import0.ɵnov(v,15).ngClassDirty;
    const currVal_6:any = import0.ɵnov(v,15).ngClassValid;
    const currVal_7:any = import0.ɵnov(v,15).ngClassInvalid;
    const currVal_8:any = import0.ɵnov(v,15).ngClassPending;
    ck(v,10,0,currVal_2,currVal_3,currVal_4,currVal_5,currVal_6,currVal_7,currVal_8);
  });
}
export function View_JhiConfigurationComponent_0(l:any):import0.ɵViewDefinition {
  return import0.ɵvid(0,[
    import0.ɵpid(0,import5.JhiOrderByPipe,([] as any[])),
    import0.ɵpid(0,import6.JhiPureFilterPipe,([] as any[])),
    (l()(),import0.ɵand(16777216,(null as any),(null as any),1,(null as any),View_JhiConfigurationComponent_1)),
    import0.ɵdid(16384,(null as any),0,import1.NgIf,[
      import0.ViewContainerRef,
      import0.TemplateRef
    ]
      ,{ngIf: [
        0,
        'ngIf'
      ]
    },(null as any)),
    (l()(),import0.ɵted((null as any),['\n']))
  ]
  ,(ck,v) => {
    var co:import7.JhiConfigurationComponent = v.component;
    const currVal_0:any = (co.allConfiguration && co.configuration);
    ck(v,3,0,currVal_0);
  },(null as any));
}
function View_JhiConfigurationComponent_Host_0(l:any):import0.ɵViewDefinition {
  return import0.ɵvid(0,[
    (l()(),import0.ɵeld(0,(null as any),(null as any),1,'jhi-configuration',([] as any[]),(null as any),(null as any),(null as any),View_JhiConfigurationComponent_0,RenderType_JhiConfigurationComponent)),
    import0.ɵdid(114688,(null as any),0,import7.JhiConfigurationComponent,[import8.JhiConfigurationService],(null as any),(null as any))
  ]
  ,(ck,v) => {
    ck(v,1,0);
  },(null as any));
}
export const JhiConfigurationComponentNgFactory:import0.ComponentFactory<import7.JhiConfigurationComponent> = import0.ɵccf('jhi-configuration',import7.JhiConfigurationComponent,View_JhiConfigurationComponent_Host_0,{},{},([] as any[]));
//# sourceMappingURL=data:application/json;base64,eyJmaWxlIjoiL1VzZXJzL21pbGFuZGVrZXQvd29yay91cHdvcmsvc3JjL21haW4vd2ViYXBwL2FwcC9hZG1pbi9jb25maWd1cmF0aW9uL2NvbmZpZ3VyYXRpb24uY29tcG9uZW50Lm5nZmFjdG9yeS50cyIsInZlcnNpb24iOjMsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm5nOi8vL1VzZXJzL21pbGFuZGVrZXQvd29yay91cHdvcmsvc3JjL21haW4vd2ViYXBwL2FwcC9hZG1pbi9jb25maWd1cmF0aW9uL2NvbmZpZ3VyYXRpb24uY29tcG9uZW50LnRzIiwibmc6Ly8vVXNlcnMvbWlsYW5kZWtldC93b3JrL3Vwd29yay9zcmMvbWFpbi93ZWJhcHAvYXBwL2FkbWluL2NvbmZpZ3VyYXRpb24vY29uZmlndXJhdGlvbi5jb21wb25lbnQuaHRtbCIsIm5nOi8vL1VzZXJzL21pbGFuZGVrZXQvd29yay91cHdvcmsvc3JjL21haW4vd2ViYXBwL2FwcC9hZG1pbi9jb25maWd1cmF0aW9uL2NvbmZpZ3VyYXRpb24uY29tcG9uZW50LnRzLkpoaUNvbmZpZ3VyYXRpb25Db21wb25lbnRfSG9zdC5odG1sIl0sInNvdXJjZXNDb250ZW50IjpbIiAiLCI8ZGl2ICpuZ0lmPVwiYWxsQ29uZmlndXJhdGlvbiAmJiBjb25maWd1cmF0aW9uXCI+XG4gICAgPGgyIGpoaVRyYW5zbGF0ZT1cImNvbmZpZ3VyYXRpb24udGl0bGVcIj5Db25maWd1cmF0aW9uPC9oMj5cblxuICAgIDxzcGFuIGpoaVRyYW5zbGF0ZT1cImNvbmZpZ3VyYXRpb24uZmlsdGVyXCI+RmlsdGVyIChieSBwcmVmaXgpPC9zcGFuPiA8aW5wdXQgdHlwZT1cInRleHRcIiBbKG5nTW9kZWwpXT1cImZpbHRlclwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCI+XG4gICAgPGxhYmVsPlNwcmluZyBjb25maWd1cmF0aW9uPC9sYWJlbD5cbiAgICA8dGFibGUgY2xhc3M9XCJ0YWJsZSB0YWJsZS1zdHJpcGVkIHRhYmxlLWJvcmRlcmVkIHRhYmxlLXJlc3BvbnNpdmUgZC10YWJsZVwiPlxuICAgICAgICA8dGhlYWQ+XG4gICAgICAgIDx0cj5cbiAgICAgICAgICAgIDx0aCBjbGFzcz1cInctNDBcIiAoY2xpY2spPVwib3JkZXJQcm9wID0gJ3ByZWZpeCc7IHJldmVyc2U9IXJldmVyc2VcIj48c3BhbiBqaGlUcmFuc2xhdGU9XCJjb25maWd1cmF0aW9uLnRhYmxlLnByZWZpeFwiPlByZWZpeDwvc3Bhbj48L3RoPlxuICAgICAgICAgICAgPHRoIGNsYXNzPVwidy02MFwiIChjbGljayk9XCJvcmRlclByb3AgPSAncHJvcGVydGllcyc7IHJldmVyc2U9IXJldmVyc2VcIj48c3BhbiBqaGlUcmFuc2xhdGU9XCJjb25maWd1cmF0aW9uLnRhYmxlLnByb3BlcnRpZXNcIj5Qcm9wZXJ0aWVzPC9zcGFuPjwvdGg+XG4gICAgICAgIDwvdHI+XG4gICAgICAgIDwvdGhlYWQ+XG4gICAgICAgIDx0Ym9keT5cbiAgICAgICAgPHRyICpuZ0Zvcj1cImxldCBlbnRyeSBvZiAoY29uZmlndXJhdGlvbiB8IHB1cmVGaWx0ZXI6ZmlsdGVyOidwcmVmaXgnIHwgb3JkZXJCeTpvcmRlclByb3A6cmV2ZXJzZSlcIj5cbiAgICAgICAgICAgIDx0ZD48c3Bhbj57e2VudHJ5LnByZWZpeH19PC9zcGFuPjwvdGQ+XG4gICAgICAgICAgICA8dGQ+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInJvd1wiICpuZ0Zvcj1cImxldCBrZXkgb2Yga2V5cyhlbnRyeS5wcm9wZXJ0aWVzKVwiPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTRcIj57e2tleX19PC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtOFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJmbG9hdC1yaWdodCBiYWRnZSBiYWRnZS1kZWZhdWx0IGJyZWFrXCI+e3tlbnRyeS5wcm9wZXJ0aWVzW2tleV0gfCBqc29ufX08L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC90ZD5cbiAgICAgICAgPC90cj5cbiAgICAgICAgPC90Ym9keT5cbiAgICA8L3RhYmxlPlxuICAgIDxkaXYgKm5nRm9yPVwibGV0IGtleSBvZiBrZXlzKGFsbENvbmZpZ3VyYXRpb24pXCI+XG4gICAgICAgIDxsYWJlbD48c3Bhbj57e2tleX19PC9zcGFuPjwvbGFiZWw+XG4gICAgICAgIDx0YWJsZSBjbGFzcz1cInRhYmxlIHRhYmxlLXNtIHRhYmxlLXN0cmlwZWQgdGFibGUtYm9yZGVyZWQgdGFibGUtcmVzcG9uc2l2ZSBkLXRhYmxlXCI+XG4gICAgICAgICAgICA8dGhlYWQ+XG4gICAgICAgICAgICA8dHI+XG4gICAgICAgICAgICAgICAgPHRoIGNsYXNzPVwidy00MFwiPlByb3BlcnR5PC90aD5cbiAgICAgICAgICAgICAgICA8dGggY2xhc3M9XCJ3LTYwXCI+VmFsdWU8L3RoPlxuICAgICAgICAgICAgPC90cj5cbiAgICAgICAgICAgIDwvdGhlYWQ+XG4gICAgICAgICAgICA8dGJvZHk+XG4gICAgICAgICAgICA8dHIgKm5nRm9yPVwibGV0IGl0ZW0gb2YgYWxsQ29uZmlndXJhdGlvbltrZXldXCI+XG4gICAgICAgICAgICAgICAgPHRkIGNsYXNzPVwiYnJlYWtcIj57e2l0ZW0ua2V5fX08L3RkPlxuICAgICAgICAgICAgICAgIDx0ZCBjbGFzcz1cImJyZWFrXCI+XG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiZmxvYXQtcmlnaHQgYmFkZ2UgYmFkZ2UtZGVmYXVsdCBicmVha1wiPnt7aXRlbS52YWx9fTwvc3Bhbj5cbiAgICAgICAgICAgICAgICA8L3RkPlxuICAgICAgICAgICAgPC90cj5cbiAgICAgICAgICAgIDwvdGJvZHk+XG4gICAgICAgIDwvdGFibGU+XG4gICAgPC9kaXY+XG48L2Rpdj5cbiIsIjxqaGktY29uZmlndXJhdGlvbj48L2poaS1jb25maWd1cmF0aW9uPiJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O01DZ0JnQjtRQUFBO1FBQUE7TUFBQTtJQUFBO0lBQTREO01BQ3hEO1FBQUE7UUFBQTtNQUFBO0lBQUE7SUFBc0I7TUFBQTtNQUFBO0lBQUE7SUFBQTtJQUFhO01BQ25DO1FBQUE7UUFBQTtNQUFBO0lBQUE7SUFBc0I7TUFDbEI7UUFBQTtRQUFBO01BQUE7SUFBQTtJQUFvRDtNQUFBO01BQUE7SUFBQTtJQUFBO2dCQUFBO0lBQXVDO0lBQ3pGOzs7SUFIZ0I7SUFBQTtJQUVrQztJQUFBOzs7OztJQU5wRTtJQUFtRztJQUMvRjtJQUFJO0lBQU07TUFBQTtNQUFBO0lBQUE7SUFBQTtJQUE0QjtJQUN0QztJQUFJO0lBQ0E7Z0JBQUE7Ozs7SUFBQTtPQUFBO1FBQUE7UUFBQTtNQUFBO0lBQUE7SUFLTTtJQUNMOzs7O0lBTmdCO0lBQWpCLFNBQWlCLFNBQWpCOztJQUZNO0lBQUE7Ozs7O0lBc0JWO0lBQStDO01BQzNDO1FBQUE7UUFBQTtNQUFBO0lBQUE7SUFBa0I7TUFBQTtNQUFBO0lBQUE7SUFBQTtJQUFpQjtNQUNuQztRQUFBO1FBQUE7TUFBQTtJQUFBO0lBQWtCO01BQ2Q7UUFBQTtRQUFBO01BQUE7SUFBQTtJQUFvRDtNQUFBO01BQUE7SUFBQTtJQUFBO0lBQW1CO0lBQ3RFOzs7SUFIYTtJQUFBO0lBRXNDO0lBQUE7Ozs7O0lBYnBFO0lBQWdEO0lBQzVDO0lBQU87SUFBTTtNQUFBO01BQUE7SUFBQTtJQUFBO0lBQXNCO01BQ25DO1FBQUE7UUFBQTtNQUFBO0lBQUE7SUFBb0Y7SUFDaEY7SUFBTztJQUNQO0lBQUk7TUFDQTtRQUFBO1FBQUE7TUFBQTtJQUFBO0lBQWlCO0lBQWE7TUFDOUI7UUFBQTtRQUFBO01BQUE7SUFBQTtJQUFpQjtJQUFVO0lBQzFCO0lBQ0c7SUFDUjtJQUFPO0lBQ1A7Z0JBQUE7Ozs7SUFBQTtPQUFBO1FBQUE7UUFBQTtNQUFBO0lBQUE7SUFLSztJQUNHO0lBQ0o7Ozs7SUFQQTtJQUFKLFVBQUksU0FBSjs7SUFUUztJQUFBOzs7OztJQTNCckI7SUFBK0M7TUFDM0M7UUFBQTtRQUFBO01BQUE7SUFBQTtrQkFBQTtRQUFBO1FBQUE7TUFBQTtJQUFBO0lBQXVDO0lBQWtCO01BRXpEO1FBQUE7UUFBQTtNQUFBO0lBQUE7a0JBQUE7UUFBQTtRQUFBO01BQUE7SUFBQTtJQUEwQztJQUF5QjtJQUFDO01BQUE7UUFBQTtRQUFBO01BQUE7O01BQUE7UUFBQTtRQUFBO01BQUE7O0lBQUE7S0FBQTtNQUFBO1FBQUE7UUFBQTtRQUFBO01BQUE7O01BQUE7UUFBQTtRQUFBO1FBQUE7TUFBQTs7TUFBQTtRQUFBO1FBQUE7UUFBQTtNQUFBOztNQUFBO1FBQUE7UUFBQTtRQUFBO01BQUE7O01BQUE7UUFBQTtRQUFBO1FBQUE7TUFBQTs7TUFBQTtRQUFBO1FBQUE7UUFBQTtNQUFBOztNQUFBO1FBQUE7UUFBQTtRQUFBO01BQUE7O0lBQUE7S0FBQTtNQUFBO1FBQUE7UUFBQTtNQUFBOztNQUFBO1FBQUE7UUFBQTtNQUFBOztNQUFBO1FBQUE7UUFBQTtNQUFBOztNQUFBO1FBQUE7UUFBQTtNQUFBOztNQUFBO1FBQUE7UUFBQTtNQUFBOztJQUFBO0tBQUE7TUFBQTtNQUFBO01BQUE7UUFBQTtRQUFBO01BQUE7TUFBQTtRQUFBO1FBQUE7TUFBQTtNQUFBO1FBQUE7UUFBQTtNQUFBO01BQUE7UUFBQTtRQUFBO01BQUE7TUFBbUI7UUFBQTtRQUFBO01BQUE7TUFBbkI7SUFBQTtnQkFBQTs7O01BQUE7UUFBQTs7TUFBQTs7SUFBQTtLQUFBO2dCQUFBO01BQUE7SUFBQTtnQkFBQTtNQUFBO1FBQUE7UUFBQTtNQUFBOztNQUFBO1FBQUE7UUFBQTtNQUFBOztNQUFBO1FBQUE7UUFBQTtNQUFBOztNQUFBO1FBQUE7O01BQUE7O0lBQUE7T0FBQTtRQUFBO1FBQUE7TUFBQTtJQUFBO2dCQUFBO2dCQUFBO0lBQTZEO0lBQ2pJO0lBQU87SUFBNEI7TUFDbkM7UUFBQTtRQUFBO01BQUE7SUFBQTtJQUEyRTtJQUN2RTtJQUFPO0lBQ1A7SUFBSTtNQUNBO1FBQUE7UUFBQTtNQUFBO01BQUE7UUFBQTtRQUFBO01BQUE7SUFBQTtNQUFBO01BQUE7TUFBaUI7UUFBQTtRQUFBO1FBQUE7TUFBQTtNQUFqQjtJQUFBO01BQWtFO1FBQUE7UUFBQTtNQUFBO0lBQUE7a0JBQUE7UUFBQTtRQUFBO01BQUE7SUFBQTtJQUFnRDtJQUFrQjtNQUNwSTtRQUFBO1FBQUE7TUFBQTtNQUFBO1FBQUE7UUFBQTtNQUFBO0lBQUE7TUFBQTtNQUFBO01BQWlCO1FBQUE7UUFBQTtRQUFBO01BQUE7TUFBakI7SUFBQTtNQUFzRTtRQUFBO1FBQUE7TUFBQTtJQUFBO2tCQUFBO1FBQUE7UUFBQTtNQUFBO0lBQUE7SUFBb0Q7SUFBc0I7SUFDL0k7SUFDRztJQUNSO0lBQU87SUFDUDtnQkFBQTs7OztJQUFBO09BQUE7UUFBQTtRQUFBO01BQUE7SUFBQTtnQkFBSTtnQkFBQTtJQVVDO0lBQ0c7SUFDSjtJQUNSO2dCQUFBOzs7O0lBQUE7T0FBQTtRQUFBO1FBQUE7TUFBQTtJQUFBO0lBa0JNOzs7O0lBM0NGO0lBQUosU0FBSSxTQUFKO0lBRU07SUFBTixTQUFNLFNBQU47SUFBdUY7SUFBbkIsVUFBbUIsU0FBbkI7SUFLWTtJQUFOLFVBQU0sVUFBTjtJQUNVO0lBQU4sVUFBTSxVQUFOO0lBSXRFO0lBQUosVUFBSSxVQUFKO0lBYUM7SUFBTCxVQUFLLFVBQUw7O0lBdkJvRTtJQUFBO0lBQUE7SUFBQTtJQUFBO0lBQUE7SUFBQTtJQUFBLFVBQUEscUVBQUE7Ozs7Ozs7SUFIeEU7Z0JBQUE7OztJQUFBO09BQUE7UUFBQTtRQUFBO01BQUE7SUFBQTtJQTZDTTs7OztJQTdDRDtJQUFMLFNBQUssU0FBTDs7Ozs7SUNBQTtnQkFBQTs7O0lBQUE7OzsifQ==
